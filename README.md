## General

This is document describing a todo list management application for Android platform.

The idea is based on the project for implementing web application based on different JavaScript frameworks. Follow [this](http://todomvc.com) link to try the demos.

## Application Specifications

### First Start
Application opens with an empty screen with only one text field to type in the first task. The field has a placeholder with the following text: "What needs to be done?". 

### Add New Task
User types in the text field at the top of the app. New task is being created when user touches the "Go" or "Return" button on the opened keyboard. The field always stays at the top of the screen while added tasks can be listed down and scrolled.

As the new task created - buttons are being added for each row, to mark the task as done or delete it.

Also complete all button should appear at the top and status bar at the bottom. The status bar should contain number of active (not done) tasks and three filter buttons: 'All', 'Active', 'Completed'.

### Remove or Complete a Task
Each task should have a 'Mark Done' and Delete buttons. 

When user toches the 'Mark Done' button the task text becomes lined through and a button appears in the status bar which clears all completed tasks. Button should have the following text on it 'Clear completed (#num)', where #num is the number of finished tasks. The number automatically updates with the change of the number of the finished tasks.

### Storage
All tasks can be stored locally or in the memory while application is running.

### Requirements
* The checked in code SHOULD be LINTED and DOCUMENTED properly.
* Android 2.3, 4.0, 4.1 support.